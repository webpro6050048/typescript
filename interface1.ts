interface Rectangle1 {
    width1: number;
    height1: number;
}

interface ColoredRectangle1 extends Rectangle1 {
    color: string
}

const rectangle1: Rectangle1 = {
    width1: 20,
    height1: 10
}
console.log(rectangle1)

const coloredRectangle1: ColoredRectangle1 = {
    width1: 20,
    height1: 10,
    color: "red"
}

console.log(coloredRectangle1)