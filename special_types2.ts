let w: unknown = 1;
w = "string";
w = {
    runANonExistrntMethod:() => {
        console.log("I think therefore I am");
    }
} as { runANonExistrntMethod:() => void}

if(typeof w === 'object' && w!== null) {
    (w as { runANonExistrntMethod: ( ) => void}).runANonExistrntMethod();
}